# SMASHUP: Simple Modeling and Attestation of Software and Hardware Using Proverif

## Description

smashup.py is a python script that translates assembly code into a ProVerif specification

Currently only s43 assembly code is accepted.

## Try it

To test it with the example provided:

```
$ ./smashup.py -o test.pv -s .do_mac.call --clauses examples/test.s43
$ proverif test.pv
```

You can try to modify `examples/test.s43` and `examples/test.hdl` to see how it impacts the verification

## Adding other HW modules

SMASHUP is being designed with flexibility in mind. Here is what one should do in order to add a new type of hardware module. Please keep in mind that SMASHUP aims to model an instruction accurate version of a processor, together with custom hardware pieces that impacts the way the software is executed. Therefore the capacities of a hardware module are described as potential modifications of the state of the system between or during instructions.

The subdirectory `parser/hwmodules` contains one python file per existing module. You only need to add one named `MODULE.py` to use a module `MODULE` in the hardware description file.

This file should define a class named `MODULE` which inherits from `HWModule`. A module can define new types and functions for its use thanks to the `typeDecl` and `funcDecl` methods. It can also add some variables to the state of the system and create ProVerif processes. Lastly a module should also list the instructions it can handle. More details are to come, until then you can take example from the existing modules.
