#  This software is a python script that enables to translate assembly
#  instructions into a ProVerif specification that ProVerif is able to
#  prove.

#  This software is governed by the CeCILL  license under French law and
#  abiding by the rules of distribution of free software.  You can  use,
#  modify and/ or redistribute the software under the terms of the CeCILL
#  license as circulated by CEA, CNRS and INRIA at the following URL
#  "http://www.cecill.info".

#  As a counterpart to the access to the source code and  rights to copy,
#  modify and redistribute granted by the license, users are provided only
#  with a limited warranty  and the software's author,  the holder of the
#  economic rights,  and the successive licensors  have only  limited
#  liability.

#  In this respect, the user's attention is drawn to the risks associated
#  with loading,  using,  modifying and/or developing or reproducing the
#  software by the user in light of its specific status of free software,
#  that may mean  that it is complicated to manipulate,  and  that  also
#  therefore means  that it is reserved for developers  and  experienced
#  professionals having in-depth computer knowledge. Users are therefore
#  encouraged to load and test the software's suitability as regards their
#  requirements in conditions enabling the security of their systems and/or
#  data to be ensured and,  more generally, to use and operate it in the
#  same conditions as regards security.

#  The fact that you are presently reading this means that you have had
#  knowledge of the CeCILL license and that you accept its terms.

import ply.lex as lex
import ply.yacc as yacc
import sys
import os

class ASTNode:
    def describe(self):
        return ""

class ASTProgram (ASTNode):
    def __init__(self, decl):
        self.decls = [decl]

    def addDeclaration(self, decl):
        self.decls.append(decl)

    def getSection (self, name=None):
        isSection = [d.isSection() for d in self.decls]
        #print name
        #print '\n'.join([self.decls[i].args[0] for i in range(len(self.decls)) if isSection[i]])
        if not any(isSection) and name is None:
            return self.decls

        if name is not None:
            searchedSection = [d.isSection() and len(d.args) > 0 and d.args[0] == name for d in self.decls]
            if not any(searchedSection):
                sys.exit('No section named \'%s\' in input file...' % name)
            for i in range(searchedSection.index(True)):
                isSection[i] = False

        begining = isSection.index(True)
        if any(isSection[begining+1:]):
            end = begining + 1 + isSection[begining+1:].index(True)
            return self.decls[begining+1:end]
        return self.decls[begining+1:]

    def describe (self):
        return '\n'.join([d.describe() for d in self.decls])

class ASTDeclaration (ASTNode):
    def isSection(self):
        return False
    def isInstruction(self):
        return False
    def isWord(self):
        return False
    def isLabel(self):
        return False

class SectionDecl (ASTDeclaration):
    def __init__ (self, l):
        self.args = l
    def isSection(self):
        return True
    def describe (self):
        return '.section ' + ','.join(self.args)

class GlobalDecl (ASTDeclaration):
    def __init__ (self, s):
        self.name = s
    def describe (self):
        return '.global ' + self.name

class WordDecl (ASTDeclaration):
    def __init__ (self, s):
        self.s = s
    def isWord(self):
        return True
    def describe(self):
        return '.word ' + self.s

class LabelDecl (ASTDeclaration):
    def __init__ (self, s):
        self.name = s
        self.addr = 0
    def isLabel(self):
        return True
    def describe (self):
        return self.name + ':'

class InstrDecl (ASTDeclaration):
    def __init__ (self, i, ops):
        self.instr = i
        self.ops = ops
        self.index = 0

    def isInstruction(self):
        return True

    def isByte (self):
        return False

    def describe (self):
        if len(self.ops) == 0:
            s = '    {:s}'
            args = [self.getinstr()]
        elif len(self.ops) == 1:
            s = '    {:<8s}{:s}'
            args = [self.getinstr(), self.ops[0].describe()]
        elif len(self.ops) == 2:
            s = '    {:<8s}{:<16s}{:s}'
            args = [self.getinstr(), self.ops[0].describe() + ',', self.ops[1].describe()]
        return s.format(*args)

    def getinstr(self):
        return self.instr.lower()


class WordInstr (InstrDecl):
    pass

class ByteInstr (InstrDecl):
    def isByte (self):
        return True
    def getinstr(self):
        return self.instr.lower() + '.b'

class Operand:
    def isLabel (self):
        return False
    def isImm (self):
        return False
    def isReg (self):
        return False
    def isInd (self):
        return False
    def describe(self):
        return ""

class LabelOp (Operand):
    def __init__ (self, name):
        self.name = name

    def isLabel (self):
        return True

    def describe(self):
        return self.name

class ImmOp (Operand):
    def __init__ (self, value):
        self.value = value

    def isImm (self):
        return True

    def describe (self):
        return '#{:#06x}'.format(self.value)

class RegOp (Operand):
    def __init__ (self, num):
        self.num = num

    def isReg (self):
        return True

    def describe (self):
        return 'r'+str(self.num)

class IndOp (Operand):
    def __init__ (self, num):
        self.num = num

    def isInd (self):
        return True

    def describe (self):
        return '@r' + str(self.num)
        
instr = {
    'nop'  : 'NOP',
#        'rrc'  : 'RRC',
#        'swpb' : 'SWPB',
#        'rra'  : 'RRA',
#        'sxt'  : 'SXT',
#        'push' : 'PUSH',
#        'call' : 'CALL',
#        'reti' : 'RETI',
#        'jne'  : 'JNE',
#        'jnz'  : 'JNE',
    'jeq'  : 'JEQ',
    'jz'   : 'JEQ',
#        'jnc'  : 'JNC',
#        'jlo'  : 'JNC',
#        'jc'   : 'JC',
#        'jhs'  : 'JC',
#        'jn'   : 'JN',
#        'jge'  : 'JGE',
#        'jl'   : 'JL',
    'jmp'  : 'JMP',
    'mov'  : 'MOV',
    'add'  : 'ADD',
#        'addc' : 'ADDC',
#        'subc' : 'SUBC',
#        'sub'  : 'SUB',
#        'dadd' : 'DADD',
#        'bit'  : 'BIT',
#        'bic'  : 'BIC',
#        'bis'  : 'BIS',
#        'xor'  : 'XOR',
#        'and'  : 'AND',
    'eint' : 'EINT',
    'dint' : 'DINT',
    'cmp'  : 'CMP'
}
instr_no_dup = {
    'nop'  : 'NOP',
#        'rrc'  : 'RRC',
#        'swpb' : 'SWPB',
#        'rra'  : 'RRA',
#        'sxt'  : 'SXT',
#        'push' : 'PUSH',
#        'call' : 'CALL',
#        'reti' : 'RETI',
#        'jne'  : 'JNE',
    'jeq'  : 'JEQ',
#        'jlo'  : 'JNC',
#        'jhs'  : 'JC',
#        'jn'   : 'JN',
#        'jge'  : 'JGE',
#        'jl'   : 'JL',
    'jmp'  : 'JMP',
    'mov'  : 'MOV',
    'add'  : 'ADD',
#        'addc' : 'ADDC',
#        'subc' : 'SUBC',
#        'sub'  : 'SUB',
#        'dadd' : 'DADD',
#        'bit'  : 'BIT',
#        'bic'  : 'BIC',
#        'bis'  : 'BIS',
#        'xor'  : 'XOR',
#        'and'  : 'AND',
    'eint' : 'EINT',
    'dint' : 'DINT',
    'cmp'  : 'CMP'
}

class S43Lexer:

    reserved_no_dup = dict(instr_no_dup.items() + [(key+'.b', instr_no_dup[key]+'B') for key in instr_no_dup.keys()] + [
        ('.section' , 'SECTION'),
        ('.global'  , 'GLOBAL'),
        ('.word'    , 'WORD')
    ])
    reserved = dict(instr.items() + [(key+'.w', instr[key]) for key in instr.keys()] + [(key+'.b', instr[key]+'B') for key in instr.keys()] + [
        ('.section' , 'SECTION'),
        ('.global'  , 'GLOBAL'),
        ('.word'    , 'WORD')
    ])

    registers = [
        'r0',
        'r1',
        'r2',
        'r3',
        'r4',
        'r5',
        'r6',
        'r7',
        'r8',
        'r9',
        'r10',
        'r11',
        'r12',
        'r13',
        'r14',
        'r15'
    ]

    tokens = [
        'INT',
        'IDENT',
        'REGISTER',
        'STRING',
        'HTAG',
        'COLON',
        'COMMA',
        'AT',
        'NEWL'
    ] + list(reserved_no_dup.values())

    t_HTAG  = r'\#'
    t_COLON = r':'
    t_COMMA = r','
    t_AT    = r'@'

    def t_comment(self, t):
        r';.*'
        pass

    def t_INT(self, t):
        r'[+-]?((0[xX][0-9a-fA-F]+)|(\d+))'
        s = t.value
        if s[0] == '-':
            s = s[1:]
            t.value = -1
        else:
            t.value = 1
            if s[0] == '+':
                s = s[1:]
        if s[:2].lower() == '0x':
            t.value *= int(s[2:], 16)
        else:
            t.value *= int(s)
        return t

    def t_IDENT(self, t):
        r'[a-zA-Z0-9_.]+'
        t.type = self.reserved.get(t.value.lower(), 'IDENT')
        if t.type != 'IDENT':
            t.value = t.type
        elif t.value.lower() in self.registers:
            t.type = 'REGISTER'
            t.value = int(t.value[1:])
        return t

    def t_STRING (self, t):
        r'\"([^\\\n]|(\\.))*?\"'
        t.value = t.value[1:-1]
        return t
        
    def t_NEWL(self, t):
        r'\n+'
        t.lexer.lineno += len(t.value)
        return t

    t_ignore = ' \t'

    def t_error(self, t):
        print "[%d] Illegal character '%s'" % (t.lexer.lineno, t.value[0])
        t.lexer.skip(1)

    def buildLexer(self, **kwargs):
        self.lexer = lex.lex(module=self, **kwargs)

    def p_main_declaration (self, p):
        'main : main NEWL declaration'
        p[1].addDeclaration(p[3])
        p[0] = p[1]

    def p_main_newl(self, p):
        'main : main NEWL'
        p[0] = p[1]

    def p_main(self, p):
        'main : declaration'
        p[0] = ASTProgram (p[1])

    def p_declaration_section (self, p):
        'declaration : SECTION argsplus'
        p[0] = SectionDecl (p[2])

    def p_declaration_global (self, p):
        'declaration : GLOBAL IDENT'
        p[0] = GlobalDecl (p[2])

    def p_declaration_word_int (self, p):
        'declaration : WORD INT'
        p[0] = WordDecl (hex(p[2]))
    def p_declaration_word_string (self, p):
        'declaration : WORD STRING'
        p[0] = WordDecl ('"' + p[2] + '"')
    def p_declaration_word_ident (self, p):
        'declaration : WORD IDENT'
        p[0] = WordDecl (p[2])

    def p_declaration_label (self, p):
        'declaration : IDENT COLON'
        p[0] = LabelDecl (p[1])

    def p_declaration_instruction (self, p):
        'declaration : instruction'
        p[0] = p[1]

    def p_args_int (self, p):
        'args : INT'
        p[0] = hex(p[1])
    def p_args_string (self, p):
        'args : STRING'
        p[0] = '"' + p[1] + '"'
    def p_args_ident (self, p):
        'args : IDENT'
        p[0] = p[1]
    def p_args_atident (self, p):
        'args : AT IDENT'
        p[0] = '@'+p[2]
    def p_argsplus_args (self, p):
        'argsplus : args'
        p[0] = [p[1]]
    def p_argsplus (self, p):
        'argsplus : argsplus COMMA args'
        p[0] = p[1] + [p[3]]

    def p_instruction_zero (self, p):
        'instruction : NOP'
        p[0] = WordInstr(p[1], [])
    def p_instruction_zerob (self, p):
        'instruction : NOPB'
        p[0] = WordInstr(p[1][:-1], [])

    def p_instruction_eint (self, p):
        'instruction : EINT'
        p[0] = WordInstr(p[1], [])
    def p_instruction_dint (self, p):
        'instruction : DINT'
        p[0] = WordInstr(p[1], [])
    def p_instruction_eintb (self, p):
        'instruction : EINTB'
        p[0] = WordInstr(p[1][:-1], [])
    def p_instruction_dintb (self, p):
        'instruction : DINTB'
        p[0] = WordInstr(p[1][:-1], [])

    def p_instruction_one_ident (self, p):
        '''instruction : JEQ IDENT
                       | JMP IDENT'''
        p[0] = WordInstr(p[1], [LabelOp(p[2])])
    def p_instruction_oneb_ident (self, p):
        '''instruction : JEQB IDENT
                       | JMPB IDENT'''
        p[0] = ByteInstr(p[1][:-1], [LabelOp(p[2])])
    def p_instruction_one_int (self, p):
        '''instruction : JEQ HTAG INT
                       | JMP HTAG INT'''
        p[0] = WordInstr(p[1], [ImmOp(p[3])])
    def p_instruction_oneb_int (self, p):
        '''instruction : JEQB HTAG INT
                       | JMPB HTAG INT'''
        p[0] = ByteInstr(p[1][:-1], [ImmOp(p[3])])

    def p_instruction_two_reg_reg (self, p):
        '''instruction : MOV REGISTER COMMA REGISTER
                       | ADD REGISTER COMMA REGISTER
                       | CMP REGISTER COMMA REGISTER'''
        p[0] = WordInstr (p[1], [RegOp(p[2]), RegOp(p[4])])
    def p_instruction_twob_reg_reg (self, p):
        '''instruction : MOVB REGISTER COMMA REGISTER
                       | ADDB REGISTER COMMA REGISTER
                       | CMPB REGISTER COMMA REGISTER'''
        p[0] = ByteInstr (p[1][:-1], [RegOp(p[2]), RegOp(p[4])])
    def p_instruction_two_reg_ind (self, p):
        '''instruction : MOV REGISTER COMMA AT REGISTER
                       | ADD REGISTER COMMA AT REGISTER
                       | CMP REGISTER COMMA AT REGISTER'''
        p[0] = WordInstr (p[1], [RegOp(p[2]), IndOp(p[5])])
    def p_instruction_twob_reg_ind (self, p):
        '''instruction : MOVB REGISTER COMMA AT REGISTER
                       | ADDB REGISTER COMMA AT REGISTER
                       | CMPB REGISTER COMMA AT REGISTER'''
        p[0] = ByteInstr (p[1][:-1], [RegOp(p[2]), IndOp(p[5])])
    def p_instruction_two_imm_reg (self, p):
        '''instruction : MOV HTAG INT COMMA REGISTER
                       | ADD HTAG INT COMMA REGISTER
                       | CMP HTAG INT COMMA REGISTER'''
        p[0] = WordInstr (p[1], [ImmOp(p[3]), RegOp(p[5])])
    def p_instruction_twob_imm_reg (self, p):
        '''instruction : MOVB HTAG INT COMMA REGISTER
                       | ADDB HTAG INT COMMA REGISTER
                       | CMPB HTAG INT COMMA REGISTER'''
        p[0] = ByteInstr (p[1][:-1], [ImmOp(p[3]), RegOp(p[5])])
    def p_instruction_two_imm_ind (self, p):
        '''instruction : MOV HTAG INT COMMA AT REGISTER
                       | ADD HTAG INT COMMA AT REGISTER
                       | CMP HTAG INT COMMA AT REGISTER'''
        p[0] = WordInstr (p[1], [ImmOp(p[3]), IndOp(p[6])])
    def p_instruction_twob_imm_ind (self, p):
        '''instruction : MOVB HTAG INT COMMA AT REGISTER
                       | ADDB HTAG INT COMMA AT REGISTER
                       | CMPB HTAG INT COMMA AT REGISTER'''
        p[0] = ByteInstr (p[1][:-1], [ImmOp(p[3]), IndOp(p[6])])

    def p_error(self, p):
        if p is None:
            s = 'Unexpected end of file'
        else:
            s = 'Syntax error on line %d:\n' % p.lineno
            i = p.lineno
            pos = 0
            for l in open(self.filename):
                i -= 1
                if i == 0:
                    s += l
                    s += ' '*(p.lexpos-pos)
                    s += '^\n'
                    break
                pos += len(l)
        print s
        #raise SyntaxError(s)

    def buildParser(self, **kwargs):
        self.yacc = yacc.yacc(module=self, **kwargs)

    def parse_file (self, filename):
        self.filename = filename
        if self.lexer is None or self.yacc is None:
            sys.exit('Please build lexer and parser')
        self.filename = filename
        content_file = open(filename, 'r')
        content = content_file.read()

        #self.lexer.input(content)
        #while True:
        #    tok = self.lexer.token()
        #    if not tok: break      # No more input
        #    print tok

        root = self.yacc.parse(content)
        content_file.close()
        if root.__class__.__name__ == 'ASTProgram' :
            return root

        return None

def parse_file (filename):
    m = S43Lexer()
    m.buildLexer()
    tmpdir = os.path.dirname(os.path.abspath(__file__))+'/tmp'
    m.buildParser(outputdir=tmpdir, debug=0)
    root = m.parse_file(filename)
    return root
