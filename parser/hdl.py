import ply.lex as lex
import ply.yacc as yacc
import os

class HWLexer:
    tokens = [
        'INT',
        'FLOAT',
        'IDENT',
        'STRING',
        'COLON',
        'COMMA',
        'LPAREN',
        'RPAREN'
    ]

    t_COLON = r':'
    t_COMMA = r','
    t_LPAREN = r'\('
    t_RPAREN = r'\)'

    def t_comment(self, t):
        r'\#.*'
        pass

    def t_FLOAT(self, t):
        r'[+-]?(\d+\.\d*)'
        s = t.value
        if s[0] == '-':
            s = s[1:]
            t.value = -1
        else:
            t.value = 1
            if s[0] == '+':
                s = s[1:]
        t.value *= float(s)
        return t

    def t_INT(self, t):
        r'[+-]?((0[xX][0-9a-fA-F]+)|(\d+))'
        s = t.value
        if s[0] == '-':
            s = s[1:]
            t.value = -1
        else:
            t.value = 1
            if s[0] == '+':
                s = s[1:]
        if s[:2].lower() == '0x':
            t.value *= int(s[2:], 16)
        else:
            t.value *= int(s)
        return t

    def t_IDENT(self, t):
        r'[a-zA-Z0-9_]+'
        t.type = 'IDENT'
        return t

    def t_STRING (self, t):
        r'\"([^\\\n]|(\\.))*?\"'
        t.value = t.value[1:-1]
        return t
        
    def t_NEWL(self, t):
        r'\n+'
        t.lexer.lineno += len(t.value)
        return t

    t_ignore = ' \t\n'

    def t_error(self, t):
        print "[%d] Illegal character '%s'" % (t.lexer.lineno, t.value[0])
        t.lexer.skip(1)

    def buildLexer(self, **kwargs):
        self.lexer = lex.lex(module=self, **kwargs)

    def p_main_declaration (self, p):
        'main : main declaration'
        p[1].append(p[2])
        p[0] = p[1]

    def p_main (self, p):
        'main : declaration'
        p[0] = [p[1]]

    def p_declaration (self, p):
        'declaration : IDENT LPAREN parameters RPAREN'
        p[0] = (p[1], p[3])

    def p_empty(self, p):
        'empty :'
        p[0] = []

    def p_parameters_parameters_plus (self, p):
        '''parameters : empty
                      | parameters_plus'''
        p[0] = p[1]

    def p_parameters_plus_parameter (self, p):
        'parameters_plus : parameter'
        p[0] = [p[1]]

    def p_parameters_plus_comma (self, p):
        'parameters_plus : parameters_plus COMMA parameter'
        if p[3] is not None:
            p[1].append(p[3])
        p[0] = p[1]

    def p_parameter (self, p):
        '''parameter : IDENT COLON INT
                     | IDENT COLON FLOAT
                     | IDENT COLON STRING'''
        p[0] = {'name': p[1], 'value': p[3]}

    def p_parameter_empty (self, p):
        'parameter : empty'
        p[0] = None

    def p_error(self, p):
        if p is None:
            s = 'Unexpected end of file'
        else:
            s = 'Syntax error on line %d:\n' % p.lineno
            i = p.lineno
            pos = 0
            for l in open(self.filename):
                i -= 1
                if i == 0:
                    s += l
                    s += ' '*(p.lexpos-pos)
                    s += '^\n'
                    break
                pos += len(l)
        print s

    def buildParser(self, **kwargs):
        self.yacc = yacc.yacc(module=self, **kwargs)

    def parse_file (self, filename):
        self.filename = filename
        if self.lexer is None or self.yacc is None:
            sys.exit('Please build lexer and parser')
        self.filename = filename
        content_file = open(filename, 'r')
        content = content_file.read()
        mods = self.yacc.parse(content)
        content_file.close()

        return mods
        

def readHDLFile (hdlfile):
    rcount = -1
    bitwidth = -1
    memrange = 0

    m = HWLexer()
    m.buildLexer()
    tmpdir = os.path.dirname(os.path.abspath(__file__))+'/tmp'
    m.buildParser(outputdir=tmpdir, debug=0)
    modules = m.parse_file(hdlfile)
    
    mods = []
    dependancies = {}
    for mod, args in modules:
        try:
            if mod in [m.modName for m in mods]:
                raise ValueError ('Module `%s` can only be instantiated once' % mod)
            _temp = __import__('hwmodules.%s' % mod, globals(), locals(), [mod], -1)
            hwmodule = getattr(_temp, mod)
            m = hwmodule()
            for d in m.dependancies:
                dependancies[d] = mod
            for d in args:
                m.parameter(**d)
            mods.append(m)
            if mod == 'CPU':
                rcount = m.rcount
                bitwidth = m.width
            elif mod == 'MEMORY':
                memrange = 2**m.width
        except ImportError:
            raise ValueError ('Couldn\'t find HW module corresponding to module `%s`' % mod)

    if rcount == -1:
        raise ValueError ('HW description file should declare a CPU module')

    for d, mod in dependancies.items():
        if d not in [m.modName for m in mods]:
            raise ValueError ('HW module `%s` requires a module `%s`' % (mod, d))

    return rcount, bitwidth, memrange, mods
