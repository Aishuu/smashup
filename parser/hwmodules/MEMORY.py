#  This software is a python script that enables to translate assembly
#  instructions into a ProVerif specification that ProVerif is able to
#  prove.

#  This software is governed by the CeCILL  license under French law and
#  abiding by the rules of distribution of free software.  You can  use,
#  modify and/ or redistribute the software under the terms of the CeCILL
#  license as circulated by CEA, CNRS and INRIA at the following URL
#  "http://www.cecill.info".

#  As a counterpart to the access to the source code and  rights to copy,
#  modify and redistribute granted by the license, users are provided only
#  with a limited warranty  and the software's author,  the holder of the
#  economic rights,  and the successive licensors  have only  limited
#  liability.

#  In this respect, the user's attention is drawn to the risks associated
#  with loading,  using,  modifying and/or developing or reproducing the
#  software by the user in light of its specific status of free software,
#  that may mean  that it is complicated to manipulate,  and  that  also
#  therefore means  that it is reserved for developers  and  experienced
#  professionals having in-depth computer knowledge. Users are therefore
#  encouraged to load and test the software's suitability as regards their
#  requirements in conditions enabling the security of their systems and/or
#  data to be ensured and,  more generally, to use and operate it in the
#  same conditions as regards security.

#  The fact that you are presently reading this means that you have had
#  knowledge of the CeCILL license and that you accept its terms.

from . import *

class MEMORY (HWModule):
    def __init__(self):
        super(MEMORY, self).__init__('MEMORY')
        self.addParameter({'name': "width", 'type': int, 'default': 4,
                    'help':'bitwidth of memory addresses. Default is 0 (no memory)'})

        self.addInstruction('MOV', self._astMOV)

    @ifInstantiated
    def state(self, parser):
        return 'm: mem', 'm'

    @ifInstantiated
    def stateInit(self, parser):
        return 'm: mem', 'm'

    @ifInstantiated
    def typeDecl(self, parser):
        declarations = []
        declarations.append(parser.ASTNode        ('type mem.'))
        return declarations

    @ifInstantiated
    def funcDecl(self, parser, rcount, bitwidth, memrange):
        declarations = []
        declarations.append(parser.ASTComment(['', '(* Memory Manipulation *)']))
        declarations.append(parser.ASTNode    ('fun mem_comp (' + ', '.join(['int'] * memrange) + '): mem [data].'))
        declarations.append(parser.ASTNode    ('reduc' + ';'.join(['\n    %s']*memrange) + ' [private].', [parser.ASTNode('forall '+ ', '.join(['m'+str(j)+': int' for j in range(memrange)]) + '; mem_read (mem_comp (' + ', '.join(['m'+str(j) for j in range(memrange)]) + '), intcomp (' + ', '.join(parser.bitlist(i, bitwidth)) + ')) = m' + str(i)) for i in range(memrange)]))
        declarations.append(parser.ASTNode    ('reduc' + ';'.join(['\n    %s']*memrange) + ' [private].', [parser.ASTNode('forall '+ ', '.join(['m'+str(j)+': int' for j in range(memrange)]) + ', v: int; mem_write (mem_comp (' + ', '.join(['m'+str(j) for j in range(memrange)]) + '), intcomp(' + ', '.join(parser.bitlist(i, bitwidth)) + '), v) = mem_comp (' + ', '.join(['m'+str(j) for j in range(i)] + ['v'] + ['m'+str(j) for j in range(i+1,memrange)]) + ')') for i in range(memrange)]))
        return declarations

    def _astMOV(self, parser, args, rcount, bitwidth, memrange, state, id):
        state = [s[1] for s in state]
        name = 'MOV_'
        if args[0].isImm():
            name += str(args[0].value)
            args[0] = 'O'+str(args[0].value)+'()'
        elif args[0].isReg():
            name += 'r' + str(args[0].num)
            args[0] = 'r' + str(args[0].num)
        else:
            return None, None
        name += '_'
        if args[1].isInd():
            name += 'at_r' + str(args[1].num)
            args[1] = 'r' + str(args[1].num)
            state[id] = 'mem_write (m, ' + args[1] + ', ' + args[0] + ')'
            return (name, parser.ASTNode ('out (ch, dummyenc(' + ', '.join(state) + '))'))
        else:
            return None, None
