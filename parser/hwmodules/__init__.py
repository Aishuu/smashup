#  This software is a python script that enables to translate assembly
#  instructions into a ProVerif specification that ProVerif is able to
#  prove.

#  This software is governed by the CeCILL  license under French law and
#  abiding by the rules of distribution of free software.  You can  use,
#  modify and/ or redistribute the software under the terms of the CeCILL
#  license as circulated by CEA, CNRS and INRIA at the following URL
#  "http://www.cecill.info".

#  As a counterpart to the access to the source code and  rights to copy,
#  modify and redistribute granted by the license, users are provided only
#  with a limited warranty  and the software's author,  the holder of the
#  economic rights,  and the successive licensors  have only  limited
#  liability.

#  In this respect, the user's attention is drawn to the risks associated
#  with loading,  using,  modifying and/or developing or reproducing the
#  software by the user in light of its specific status of free software,
#  that may mean  that it is complicated to manipulate,  and  that  also
#  therefore means  that it is reserved for developers  and  experienced
#  professionals having in-depth computer knowledge. Users are therefore
#  encouraged to load and test the software's suitability as regards their
#  requirements in conditions enabling the security of their systems and/or
#  data to be ensured and,  more generally, to use and operate it in the
#  same conditions as regards security.

#  The fact that you are presently reading this means that you have had
#  knowledge of the CeCILL license and that you accept its terms.

def ifInstantiated(Fct):
    def wrapped(*c, **d):
        if not c[0].instantiated:
            raise ValueError('Module %s has not been instantiated' % c[0].modName)
        return Fct(*c, **d)
    return wrapped

class HWModule (object):
    def __init__(self, modName):
        self.modName = modName
        self._parameters = {}
        self.instantiated = False
        self.instrDic = {}
        self.dependancies = []

    def addParameter(self, d):
        if 'name' in d.keys() and 'help' in d.keys() and ('type' in d.keys() or 'typecheck' in d.keys()):
            self._parameters[d['name']] = d
            if hasattr(self, d['name']):
                delattr(self, d['name'])
            if 'default' in d.keys():
                if 'typecheck' in d.keys() and not d['typecheck'](d['default']):
                    raise TypeError ('Parameter %s of module %s has incorrect default value `%s`' % (d['name'], self.modName, d['default']))
                elif 'type' in d.keys() and type(d['default']) is not d['type']:
                    raise TypeError ('Parameter %s of module %s has default value `%s`, which is not of type `%s`' % (d['name'], self.modName, d['default'], str(d['type']).split("'")[1]))
                else:
                    setattr(self, d['name'], d['default'])
        else:
            raise ValueError('New parameter for module %s should at least define `name`, `help` and `type` or `typecheck`' % self.modName)

    def parameter(self, name, value):
        if name not in self._parameters.keys():
            raise ValueError('Module %s has no parameter named `%s`' % (self.modName, name))
        d = self._parameters[name]
        if 'typecheck' in d.keys() and not d['typecheck'](value):
            raise TypeError ('Parameter %s of module %s has incorrect value `%s`' % (name, self.modName, str(value)))
        elif 'type' in d.keys() and type(value) is not d['type']:
            raise TypeError ('Parameter %s of module %s should be of type `%s`' % (name, self.modName, str(d['type']).split("'")[1]))
        else:
            setattr(self, name, value)

    def addInstruction(self, name, func):
        self.instrDic[name] = func

    def instantiate(self):
        for d in self._parameters.keys():
            if not hasattr(self, d):
                raise ValueError('Parameter `%s` has not been provided' % d['name'])
        self.instantiated = True
        
    @ifInstantiated
    def state(self, parser):
        return None, None

    @ifInstantiated
    def stateInit(self, parser):
        return None, None

    @ifInstantiated
    def typeDecl(self, parser):
        return []

    @ifInstantiated
    def funcDecl(self, parser, rcount, bitwidth, memrange):
        return []

    @ifInstantiated
    def processDecl(self, parser, rcount, bitwidth, memrange, stateAll, id):
        return [], []

    @ifInstantiated
    def createInstr (self, parser, instr, rcount, bitwidth, memrange, state, id):
        if instr.instr in self.instrDic:
            return self.instrDic[instr.instr](parser, instr.ops[:], rcount, bitwidth, memrange, state, id)
        return None, None
