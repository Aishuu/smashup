#  This software is a python script that enables to translate assembly
#  instructions into a ProVerif specification that ProVerif is able to
#  prove.

#  This software is governed by the CeCILL  license under French law and
#  abiding by the rules of distribution of free software.  You can  use,
#  modify and/ or redistribute the software under the terms of the CeCILL
#  license as circulated by CEA, CNRS and INRIA at the following URL
#  "http://www.cecill.info".

#  As a counterpart to the access to the source code and  rights to copy,
#  modify and redistribute granted by the license, users are provided only
#  with a limited warranty  and the software's author,  the holder of the
#  economic rights,  and the successive licensors  have only  limited
#  liability.

#  In this respect, the user's attention is drawn to the risks associated
#  with loading,  using,  modifying and/or developing or reproducing the
#  software by the user in light of its specific status of free software,
#  that may mean  that it is complicated to manipulate,  and  that  also
#  therefore means  that it is reserved for developers  and  experienced
#  professionals having in-depth computer knowledge. Users are therefore
#  encouraged to load and test the software's suitability as regards their
#  requirements in conditions enabling the security of their systems and/or
#  data to be ensured and,  more generally, to use and operate it in the
#  same conditions as regards security.

#  The fact that you are presently reading this means that you have had
#  knowledge of the CeCILL license and that you accept its terms.

from . import *

class CPU (HWModule):
    def __init__(self):
        super(CPU, self).__init__('CPU')
        self.addParameter({'name': "width", 'type': int, 'default': 4,
                    'help': 'bitwidth of registers. Default is 4'})
        def checkRcount (v):
            return type(v) is int and v >=3
        self.addParameter({'name': "rcount", 'typecheck': checkRcount, 'default': 3,
                    'help': 'number of available registers. This should at least be 3 (r0: PC, r1: secret, r2: SR. Default is 3'})

        self.addInstruction('NOP', self._astNOP)
        self.addInstruction('END', self._astEND)
        self.addInstruction('JMP', self._astJMP)
        self.addInstruction('JEQ', self._astJEQ)
        self.addInstruction('ADD', self._astADD)
        self.addInstruction('CMP', self._astCMP)
        self.addInstruction('MOV', self._astMOV)

    @ifInstantiated
    def state(self, parser):
        return ', '.join(['r'+str(i)+': int' for i in range(self.rcount)]), 'N(r0), ' + ', '.join(['r'+str(i) for i in range(1, self.rcount)])

    @ifInstantiated
    def stateInit(self, parser):
        return ', '.join(['r'+str(i)+': int' for i in range(2, self.rcount)]), 'O0(), secret, ' + ', '.join(['r'+str(i) for i in range(2, self.rcount)])

    @ifInstantiated
    def typeDecl(self, parser):
        declarations = []
        declarations.append(parser.ASTNode        ('type int.'))
        declarations.append(parser.ASTNode        ('type bit.'))
        return declarations

    @ifInstantiated
    def funcDecl(self, parser, rcount, bitwidth, memrange):
        declarations = []
        declarations.append(parser.ASTComment(['', '(* Numeric functions *)']))
        declarations.append(parser.ASTNode        ('const '+parser.bitrepr[0]+': bit [data].'))
        declarations.append(parser.ASTNode        ('const '+parser.bitrepr[1]+': bit [data].'))
        declarations.append(parser.ASTNode        ('fun intcomp (' + ', '.join(['bit']*self.width) + '): int [data].'))
        for i in range(2**self.width):
            declarations.append(parser.ASTNode    (('letfun O{:<3d} = intcomp ('+', '.join(parser.bitlist(i, self.width))+').').format(i)))
        declarations.append(parser.ASTComment(['']))

        declarations.append(parser.ASTNode        ('reduc' + ';'.join(['\n    N (intcomp (' + ', '.join(parser.bitlist(n, self.width)) + ')) = intcomp (' + ', '.join(parser.bitlist((n+1) % 2**self.width, self.width)) + ')' for n in range(2**self.width)]) + ' [private].'))
        return declarations

    def _astNOP(self, parser, args, rcount, bitwidth, memrange, state, id):
        state = [s[1] for s in state]
        return ('NOP', parser.ASTNode ('out (ch, dummyenc(' + ', '.join(state) + '))'))

    def _astEND(self, parser, args, rcount, bitwidth, memrange, state, id):
        state = [s[1] for s in state]
        state[id] = ', '.join(['r'+str(i) for i in range(rcount)])
        return ('END', parser.ASTNode ('out (ch, (' + ', '.join(state) + '))'))

    def _astMOV(self, parser, args, rcount, bitwidth, memrange, state, id):
        state = [s[1] for s in state]
        name = 'MOV_'
        if args[0].isImm():
            name += str(args[0].value)
            args[0] = 'O'+str(args[0].value)+'()'
        elif args[0].isReg():
            name += 'r' + str(args[0].num)
            args[0] = 'r' + str(args[0].num)
        else:
            return None, None
        name += '_'
        if args[1].isReg():
            name += 'r' + str(args[1].num)
            args[1] = args[1].num
            state[id] = 'N(r0), ' + ', '.join([args[0] if i == args[1] else 'r'+str(i) for i in range(1, rcount)])
            return (name, parser.ASTNode ('out (ch, dummyenc(' + ', '.join(state) + '))'))
        else:
            return None, None

    def _astADD(self, parser, args, rcount, bitwidth, memrange, state, id):
        state = [s[1] for s in state]
        name = 'ADD_'
        if args[0].isImm():
            args[0] = args[0].value
            if args[0] < 0:
                args[0] += 2**bitwidth
            name += str(args[0])
        else:
            return None, None
        name += '_'
        if args[1].isReg():
            args[1] = args[1].num
            name += 'r' + str(args[1])
        else:
            return None, None
        state[id] = 'N(r0), ' + ', '.join(['N('*args[0] + 'r'+str(i) + ')'*args[0] if i == args[1] else 'r'+str(i) for i in range(1, rcount)])
        return (name, parser.ASTNode ('out (ch, dummyenc(' + ', '.join(state) + '))'))

    def _astJMP(self, parser, args, rcount, bitwidth, memrange, state, id):
        state = [s[1] for s in state]
        name = 'JMP_'
        if args[0].isImm():
            name += str(args[0].value)
            args[0] = 'O'+str(args[0].value)+'()'
        else:
            return None, None
        state[id] = args[0]+', '+ ', '.join(['r'+str(i) for i in range(1, rcount)])
        return (name, parser.ASTNode ('out (ch, dummyenc(' + ', '.join(state) + '))'))

    def _astJEQ(self, parser, args, rcount, bitwidth, memrange, state, id):
        state = [s[1] for s in state]
        name = 'JEQ_'
        if args[0].isImm():
            name += str(args[0].value)
            args[0] = 'O'+str(args[0].value)+'()'
        else:
            return None, None
        #FIXME: should be a bit test
        state1 = args[0] + ', '+ ', '.join(['r'+str(i) for i in range(1, rcount)])
        state2 = 'N(r0), '+ ', '.join(['r'+str(i) for i in range(1, rcount)])
        return (name, parser.ASTNode ('if r2 = O2() then\n        out (ch, dummyenc(' + ', '.join([state1 if i == id else s for i, s in enumerate(state)]) + '))\n    else\n        out (ch, dummyenc(' + ', '.join([state2 if i == id else s for i, s in enumerate(state)]) + '))'))

    def _astCMP(self, parser, args, rcount, bitwidth, memrange, state, id):
        state = [s[1] for s in state]
        name = 'CMP_'
        if args[0].isImm():
            name += str(args[0].value)
            args[0] = 'O'+str(args[0].value)+'()'
        elif args[0].isReg():
            name += 'r' + str(args[0].num)
            args[0] = 'r' + str(args[0].num)
        else:
            return None, None
        name += '_'
        if args[1].isReg():
            name += 'r' + str(args[1].num)
            args[1] = args[1].num
        else:
            return None, None
    #FIXME: should be a bit set
        state1 = 'N(r0), r1, O2(), ' + ', '.join(['r'+str(i) for i in range(3, rcount)])
        state2 = 'N(r0), r1, O0(), ' + ', '.join(['r'+str(i) for i in range(3, rcount)])
        return (name, parser.ASTNode ('if r' + str(args[1]) + ' = ' + args[0] + ' then\n        out (ch, dummyenc(' + ', '.join([state1 if i == id else s for i, s in enumerate(state)]) + '))\n    else\n        out (ch, dummyenc(' + ', '.join([state2 if i == id else s for i, s in enumerate(state)]) + '))'))
