#  This software is a python script that enables to translate assembly
#  instructions into a ProVerif specification that ProVerif is able to
#  prove.

#  This software is governed by the CeCILL  license under French law and
#  abiding by the rules of distribution of free software.  You can  use,
#  modify and/ or redistribute the software under the terms of the CeCILL
#  license as circulated by CEA, CNRS and INRIA at the following URL
#  "http://www.cecill.info".

#  As a counterpart to the access to the source code and  rights to copy,
#  modify and redistribute granted by the license, users are provided only
#  with a limited warranty  and the software's author,  the holder of the
#  economic rights,  and the successive licensors  have only  limited
#  liability.

#  In this respect, the user's attention is drawn to the risks associated
#  with loading,  using,  modifying and/or developing or reproducing the
#  software by the user in light of its specific status of free software,
#  that may mean  that it is complicated to manipulate,  and  that  also
#  therefore means  that it is reserved for developers  and  experienced
#  professionals having in-depth computer knowledge. Users are therefore
#  encouraged to load and test the software's suitability as regards their
#  requirements in conditions enabling the security of their systems and/or
#  data to be ensured and,  more generally, to use and operate it in the
#  same conditions as regards security.

#  The fact that you are presently reading this means that you have had
#  knowledge of the CeCILL license and that you accept its terms.

from . import *

class INT_UNIT (HWModule):
    def __init__(self):
        super(INT_UNIT, self).__init__('INT_UNIT')

        self.addInstruction('EINT', self._astEINT)
        self.addInstruction('DINT', self._astDINT)

        self.dependancies = ['CPU']

    @ifInstantiated
    def state(self, parser):
        return 'gie: bit', 'gie'

    @ifInstantiated
    def stateInit(self, parser):
        return 'gie: bit', 'gie'

    @ifInstantiated
    def processDecl(self, parser, rcount, bitwidth, memrange, stateAll, id):
        declarations = []
        stateIn  = [s[1] for s in stateAll]
        stateOutPlusOne = [(s[0], s[2]) for s in stateAll]
        stateOut = ['r0, ' + ', '.join(s[1].split(', ')[1:]) if s[0] == 'CPU' else s[1] for s in stateOutPlusOne]
        declarations.append(parser.ASTNode('let call_interrupt =\n    in (ch, enc_state: bitstring);\n    let ('+', '.join(stateIn) + ') = dummydec(enc_state) in\n\n    if gie = ' + parser.bitrepr[1] + ' then\n\n    out (ch, (' + ', '.join(stateOut) + ')).\n'))
        declarations.append(parser.ASTNode('let return_interrupt =\n    in (ch, enc_state: bitstring);\n    let ('+', '.join([s.replace(':', '_orig:') for s in stateIn]) + ') = dummydec(enc_state) in\n    in (ch, (' + ', '.join([', '.join(s[1].split(', ')[1:]) if s[0] == 'CPU' else s[1] for s in stateAll]) + '));\n\n    if gie_orig = ' + parser.bitrepr[1] + ' then\n\n    out (ch, dummyenc(' + ', '.join(['N(r0_orig), ' + ', '.join(s[1].split(', ')[1:]) if s[0] == 'CPU' else s[1] for s in stateOutPlusOne]) + ')).\n'))
        return ['call_interrupt', 'return_interrupt'], declarations

    def _astEINT(self, parser, args, rcount, bitwidth, memrange, state, id):
        state = [s[1] for s in state]
        name = 'EINT'
        state[id] = parser.bitrepr[1]
        return (name, parser.ASTNode ('out (ch, dummyenc(' + ', '.join(state) + '))'))

    def _astDINT(self, parser, args, rcount, bitwidth, memrange, state, id):
        state = [s[1] for s in state]
        name = 'EINT'
        state[id] = parser.bitrepr[0]
        return (name, parser.ASTNode ('out (ch, dummyenc(' + ', '.join(state) + '))'))
