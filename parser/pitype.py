#  This software is a python script that enables to translate assembly
#  instructions into a ProVerif specification that ProVerif is able to
#  prove.

#  This software is governed by the CeCILL  license under French law and
#  abiding by the rules of distribution of free software.  You can  use,
#  modify and/ or redistribute the software under the terms of the CeCILL
#  license as circulated by CEA, CNRS and INRIA at the following URL
#  "http://www.cecill.info".

#  As a counterpart to the access to the source code and  rights to copy,
#  modify and redistribute granted by the license, users are provided only
#  with a limited warranty  and the software's author,  the holder of the
#  economic rights,  and the successive licensors  have only  limited
#  liability.

#  In this respect, the user's attention is drawn to the risks associated
#  with loading,  using,  modifying and/or developing or reproducing the
#  software by the user in light of its specific status of free software,
#  that may mean  that it is complicated to manipulate,  and  that  also
#  therefore means  that it is reserved for developers  and  experienced
#  professionals having in-depth computer knowledge. Users are therefore
#  encouraged to load and test the software's suitability as regards their
#  requirements in conditions enabling the security of their systems and/or
#  data to be ensured and,  more generally, to use and operate it in the
#  same conditions as regards security.

#  The fact that you are presently reading this means that you have had
#  knowledge of the CeCILL license and that you accept its terms.

import ply.lex as lex
import ply.yacc as yacc
import sys
import copy
import os

class ASTNode:
    def __init__ (self, format, args=None, alinea={}):
        self.format = format
        self.args = args
        self.alinea = alinea

    def describe (self, alinea=0):
        alineas = [(alinea+self.alinea.get(i) if i in self.alinea.keys() else 0) for i in range(len(self.args))] if self.args else []
        return ('    '*alinea) + (self.format % tuple([x.describe(alineas[i]) for i, x in enumerate(self.args)]) if self.args else self.format)

    def duplicate(self):
        return None

    def replaceTerminal(self, a, b):
        if self.args:
            for x in self.args:
                x.replaceTerminal(a, b)
        else:
            if self.format == a:
                self.format = b

class ASTSequence(ASTNode):
    def __init__ (self, elements, begin='', end='', delim=', '):
        self.list = elements
        self.begin = begin
        self.end = end
        self.delim = delim

    def append (self, element):
        self.list.append(element)

    def describe (self, alinea=0):
        return '    '*alinea + self.begin + (self.delim+'    '*alinea).join([x.describe(alinea) for x in self.list]) + self.end

    def duplicate(self):
        i = 0
        while i < len(self.list):
            r = self.list[i].duplicate()
            if r:
                self.list = self.list[:i+1] + [r] + self.list[i+1:]
            i += 1
        return None

    def replaceTerminal(self, a, b):
        for x in self.list:
            x.replaceTerminal(a, b)

class ASTProcess(ASTNode):
    def __init__ (self, name, process, args=None, recursivity=0):
        self.name = name
        self.process = process
        self.args = args
        self.recursivity = recursivity
        self.alreadyCopied = 0

    def describe (self, alinea=0):
        return '\n' + '    '*alinea + 'let ' + self.name + ' ' + (self.args.describe() if self.args else '()') + ' ' + ('[recursivity %d] ' % self.recursivity if self.recursivity != 0 else '') + '=\n' + self.process.describe(alinea+1) + '.'

    def duplicate(self):
        if self.recursivity == 0:
            return None

        if self.recursivity == 1:
            objcopy = None
        else:
            objcopy = copy.deepcopy(self)
            objcopy.alreadyCopied += 1
            if self.alreadyCopied != 0:
                objcopy.recursivity -= 1

        self.recursivity = 0
        if self.alreadyCopied == 0:
            self.process = ASTComment (['(* End of Recursion *)', '0'])
        else:
            self.replaceTerminal(self.name, 'r__'+self.name+'_'+str(self.alreadyCopied - 1))

        if objcopy is not None:
            self.name = 'r__'+self.name+'_'+str(self.alreadyCopied)

        if self.args:
            for n in self.args.list:
                if n.args and len(n.args) == 2:
                    name_or = n.args[0].format
                    new_name = 'r__'+name_or+'_'+str(self.alreadyCopied)
                    n.args[0].format = new_name
                    self.replaceTerminal(name_or, new_name)

        return objcopy

    def replaceTerminal(self, a, b):
        self.process.replaceTerminal(a, b)

class ASTComment(ASTNode):
    def __init__ (self, content):
        self.content = content

    def describe(self, alinea=0):
        return '    '*alinea + ('\n'+'    '*alinea).join(self.content)

    def duplicate(self):
        return None

    def replaceTerminal(self, a, b):
        pass

class ASTMain(ASTNode):
    def __init__ (self, declarations, mainproc):
        self.declarations = declarations
        self.mainproc = mainproc

    def describe (self, alinea=0):
        return (self.declarations.describe(alinea) if self.declarations else '') + '\n\n' + '    '*alinea + 'process\n' + self.mainproc.describe(alinea+1)

    def duplicate(self):
        self.declarations.duplicate()
        return None

    def replaceTerminal(self, a, b):
        self.declarations.duplicate (a, b)
        self.mainproc.duplicate (a, b)
        
class ASTMainEquiv(ASTNode):
    def __init__ (self, declarations, proc1, proc2):
        self.declarations = declarations
        self.proc1 = proc1
        self.proc2 = proc2

    def describe (self, alinea=0):
        return (self.declarations.describe(0) if self.declarations else '') + '\n\nequivalence\n' + self.proc1.describe(1) + '\n\n' + self.proc2.describe(1)

    def duplicate(self):
        pass

    def replaceTerminal(self, a, b):
        pass

class PiTypeLexer:
    # Reserved keywords
    reserved = {
        'among' : 'AMONG'  ,
        'channel' : 'CHANNEL'  ,
        'choice' : 'CHOICE'  ,
        'clauses' : 'CLAUSES'  ,
        'const' : 'CONST'  ,
        'def' : 'DEF'  ,
        'elimtrue' : 'ELIMTRUE'  ,
        'else' : 'ELSE'  ,
        'equation' : 'EQUATION'  ,
        'equivalence' : 'EQUIVALENCE'  ,
        'event' : 'EVENT'  ,
        'expand' : 'EXPAND'  ,
        'fail' : 'FAIL'  ,
        'forall' : 'FORALL'  ,
        'free' : 'FREE'  ,
        'fun' : 'FUN'  ,
        'get' : 'GET'  ,
        'if' : 'IF'  ,
        'in' : 'IN'  ,
        'insert' : 'INSERT'  ,
        'let' : 'LET'  ,
        'letfun' : 'LETFUN'  ,
        'new' : 'NEW'  ,
        'noninterf' : 'NONINTERF'  ,
        'not' : 'NOT'  ,
        'nounif' : 'NOUNIF'  ,
        'or' : 'OR'  ,
        'otherwise' : 'OTHERWISE'  ,
        'out' : 'OUT'  ,
        'param' : 'PARAM'  ,
        'phase' : 'PHASE'  ,
        'pred' : 'PRED'  ,
        'proba' : 'PROBA'  ,
        'process' : 'PROCESS'  ,
        'proof' : 'PROOF'  ,
        'putbegin' : 'PUTBEGIN'  ,
        'query' : 'QUERY'  ,
        'reduc' : 'REDUC'  ,
        'set' : 'SET'  ,
        'suchthat' : 'SUCHTHAT'  ,
        'table' : 'TABLE'  ,
        'then' : 'THEN'  ,
        'type' : 'TYPE'  ,
        'weaksecret' : 'WEAKSECRET'  ,
        'yield ' : 'YIELD',
        'recursive' : 'RECURSIVE'
    }

    # List of token names
    tokens = [
        'INT',
        'IDENT',
        'LPAREN',
        'RPAREN',
        'LBRAC',
        'RBRAC',
        'LCURL',
        'RCURL',
        'EQUAL',
        'DIFF',
        'LAND',
        'LOR',
        'COMMA',
        'COLON',
        'SEMI',
        'PERIOD',
        'INJEV',
        'QARROW',
        'EXCL',
        'STAR',
        'SLASH',
        'RARROW',
        'DARROW',
        'EQUIV',
        'PIPE',
        'LEQ'
    ] + list(reserved.values())

    # simple tokens
    t_LPAREN    = r'\('
    t_RPAREN    = r'\)'
    t_LBRAC     = r'\['
    t_RBRAC     = r'\]'
    t_LCURL     = r'{'
    t_RCURL     = r'}'
    t_EQUAL     = r'='
    t_DIFF      = r'<>'
    t_LAND      = r'&&'
    t_LOR       = r'\|\|'
    t_COMMA     = r','
    t_COLON     = r':'
    t_SEMI      = r';'
    t_PERIOD    = r'\.'
    t_INJEV     = r'inj-event'
    t_QARROW    = r'==>'
    t_EXCL      = r'!'
    t_STAR      = r'\*'
    t_SLASH     = r'/'
    t_RARROW    = r'->'
    t_DARROW    = r'<->'
    t_EQUIV     = r'<=>'
    t_PIPE      = r'\|'
    t_LEQ       = r'<='

    precedence = (
        ('left', 'QARROW'),
        ('left', 'LOR'),
        ('left', 'LAND'),
        ('left', 'EQUAL'),
        ('left', 'DIFF'),

        ('left', 'PROCESS_EXCL'),
        ('right', 'PROCESS_IT'),
        ('right', 'PROCESS_ITE'),
        ('right', 'PROCESS_LET'),
        ('right', 'PROCESS_LET_IN'),
        ('right', 'PROCESS_LET_ELSE'),
        ('left', 'PROCESS_PIPE'),
    )

    def t_comment(self, t):
        r'\(\*(.|\n)*?\*\)'
        pass

    def t_INT(self, t):
        r'\d+'
        t.value = int(t.value)
        return t

    def t_IDENT(self, t):
        r'[a-zA-Z][0-9a-zA-Z_\']*'
        t.type = self.reserved.get(t.value, 'IDENT')
        return t

    def t_newline(self, t):
        r'\n+'
        t.lexer.lineno += len(t.value)

    t_ignore = ' \t'

    def t_error(self, t):
        print "[%d] Illegal character '%s'" % (t.lexer.lineno, t.value[0])
        t.lexer.skip(1)

    def buildLexer(self, **kwargs):
        self.lexer = lex.lex(module=self, **kwargs)

    def p_main(self, p):
        'main : declarations PROCESS process'
        p[1].delim = '\n'
        p[0] = ASTMain(p[1], p[3])
    def p_mainequiv(self, p):
        'main : declarations EQUIVALENCE process process'
        p[1].delim = '\n'
        p[0] = ASTMainEquiv(p[1], p[3], p[4])

    def p_empty(self, p):
        'empty :'
        p[0] = None

    def p_terminal_ident(self, p):
        '''term        : IDENT
           pterm       : IDENT
           pattern     : IDENT
           gterm       : IDENT
           gformat     : IDENT
           typeid      : IDENT
                       | CHANNEL
           option      : IDENT
           nidecl      : IDENT
           nounifdecltmp : IDENT
           mayfailterm : FAIL'''
        p[0] = ASTNode(p[1])

    def p_seqplus(self, p):
        '''termplus    : term
                       | termplus COMMA term
           ptermplus   : pterm
                       | ptermplus COMMA pterm
           gtermplus   : gterm
                       | gtermplus COMMA gterm
           patternplus : pattern
                       | patternplus COMMA pattern
           gformatplus : gformat
                       | gformatplus COMMA gformat
           optionplus  : option
                       | optionplus COMMA option
           typeidplus  : typeid
                       | typeidplus COMMA typeid
           nideclplus  : nidecl
                       | nideclplus COMMA nidecl
           mayfailtermplus : mayfailterm
                           | mayfailtermplus COMMA mayfailterm
           declarationplus : declaration
                           | declarationplus declaration
           typedeclplus : typedecl
                        | typedeclplus COMMA typedecl
           failtypedeclplus : failtypedecl
                            | failtypedeclplus COMMA failtypedecl'''
        if len(p) == 2:
            p[0] = ASTSequence([p[1]])
        else:
            p[0] = p[1]
            p[0].append(p[len(p)-1])

    def p_seqstar(self, p):
        '''terms    : termplus
                    | empty
           pterms   : ptermplus
                    | empty
           gterms   : gtermplus
                    | empty
           typeids  : typeidplus
                    | empty
           gformats : gformatplus
                    | empty
           nidecls  : nideclplus
                    | empty
           patterns : patternplus
                    | empty
           mayfailterms : mayfailtermplus
                        | empty
           declarations : declarationplus
                        | empty
           args         : empty
           typedecls : typedeclplus
                     | empty'''
        p[0] = p[1] if p[1] else ASTSequence ([])

    def p_tuple(self, p):
        '''term         : LPAREN terms     RPAREN
           pterm        : LPAREN pterms    RPAREN
           gterm        : LPAREN gterms    RPAREN
           pattern      : LPAREN patterns  RPAREN
           gformat      : LPAREN gformats  RPAREN
           args         : LPAREN typedecls RPAREN
           optiontuple  : LBRAC  optionplus RBRAC'''
        p[0] = p[2]
        p[0].begin = p[1]
        p[0].end   = p[3]

    def p_functionCall(self, p):
        '''term    : IDENT LPAREN terms    RPAREN
           pterm   : IDENT LPAREN pterms   RPAREN
           pattern : IDENT LPAREN patterns RPAREN
           gformat : IDENT LPAREN gformats RPAREN
                   | NOT   LPAREN gformats RPAREN
           gterm   : IDENT LPAREN gterms   RPAREN PHASE INT
                   | IDENT LPAREN gterms   RPAREN
                   | EVENT LPAREN gterms   RPAREN
                   | INJEV LPAREN gterms   RPAREN'''
        if len(p) == 5:
            p[0] = ASTNode('%s (%s)', [ASTNode(p[1]), p[3]])
        else:
            p[0] = ASTNode('%s (%s) phase %s', [ASTNode(p[1]), p[3], ASTNode(str(p[6]))])

    def p_binaryOp(self, p):
        '''term    : term    EQUAL  term
                   | term    DIFF   term
                   | term    LAND   term
                   | term    LOR    term
           pterm   : pterm   EQUAL  pterm
                   | pterm   DIFF   pterm
                   | pterm   LAND   pterm
                   | pterm   LOR    pterm
           gterm   : gterm   EQUAL  gterm
                   | gterm   DIFF   gterm
                   | gterm   LAND   gterm
                   | gterm   LOR    gterm
                   | gterm   QARROW gterm
           clause  : term    RARROW term
                   | term    DARROW term
                   | term    EQUIV  term'''
        p[0] = ASTNode ('%s '+p[2]+' %s', [p[1], p[3]])

    def p_choice(self, p):
        'pterm : CHOICE LBRAC pterm COMMA pterm RBRAC'
        p[0] = ASTNode ('choice [%s, %s]', [p[3], p[5]])

    def p_identity(self, p):
        '''clause      : term
           clausestmp  : clause
           clauses     : clausestmp
           mayfailterm : term
           options     : empty
                       | optiontuple
           query       : gterm'''
        p[0] = p[1]

    def p_typedecl(self, p):
        '''typedecl     : IDENT COLON typeid
           pattern      : IDENT COLON typeid
           failtypedecl : IDENT COLON typeid
                        | IDENT COLON typeid OR FAIL'''
        p[0] = ASTNode ('%s: %s' if len(p) == 4 else '%s: %s or fail', [ASTNode (p[1]), p[3]])

    def p_unaryOp(self, p):
        '''term  : NOT LPAREN term  RPAREN
           pterm : NOT LPAREN pterm RPAREN'''
        p[0] = ASTNode ('not (%s)', [p[3]])

    def p_newpterm(self, p):
        'pterm : NEW IDENT COLON typeid SEMI pterm'
        p[0] = ASTNode ('new %s: %s;\n%s', [ASTNode(p[2]), p[4], p[6]])

    def p_ifthenelsepterm(self, p):
        '''pterm : IF pterm THEN pterm ELSE pterm
                 | IF pterm THEN pterm'''
        if len(p) == 5:
            p[0] = ASTNode('if %s then\n%s', [p[2], p[4]])
        else:
            p[0] = ASTNode('if %s then\n%s\nelse\n%s', [p[2], p[4], p[6]])

    def p_letinpterm(self, p):
        '''pterm : LET pattern EQUAL pterm IN pterm ELSE pterm
                 | LET pattern EQUAL pterm IN pterm'''
        if len(p) == 7:
            p[0] = ASTNode('let %s = %s in\n%s', [p[2], p[4], p[6]])
        else:
            p[0] = ASTNode('let %s = %s in\n%s\nelse\n%s', [p[2], p[4], p[6], p[8]])

    def p_letsuchthatpterm(self, p):
        '''pterm : LET typedeclplus SUCHTHAT pterm IN pterm ELSE pterm
                 | LET typedeclplus SUCHTHAT pterm IN pterm'''
        if len(p) == 7:
            p[0] = ASTNode('let %s suchthat %s in\n%s', [p[2], p[4], p[6]])
        else:
            p[0] = ASTNode('let %s suchthat %s in\n%s\nelse\n%s', [p[2], p[4], p[6], p[8]])

    def p_pattern_pterm(self, p):
        'pattern : EQUAL pterm'
        p[0] = ASTNode('= %s', [p[3]])

    def p_reducs(self, p):
        '''reducs  : reduc  SEMI      reducs
                   | reduc
           reduc2s : reduc2 OTHERWISE reduc2s
                   | reduc2'''
        if len(p) == 2:
            p[0] = ASTSequence([p[1]])
        else:
            p[0] = p[1]
            p[0].append(p[3])
            p[0].delim = p[2]+'\n'
    def p_reduc(self, p):
        '''reduc  : FORALL typedeclplus SEMI term EQUAL term
                  |                       term EQUAL term'''
        if len(p) == 4:
            p[0] = ASTNode('%s = %s', [p[1], p[3]])
        else:
            p[0] = ASTNode('forall %s; %s = %s', [p[2], p[4], p[6]])
    def p_reduc2(self, p):
        '''reduc2 : FORALL failtypedeclplus SEMI IDENT LPAREN mayfailterms RPAREN EQUAL mayfailterm
                  |                              IDENT LPAREN mayfailterms RPAREN EQUAL mayfailterm'''
        if len(p) == 7:
            p[0] = ASTNode('%s (%s) = %s', [ASTNode(p[1]), p[3], p[6]])
        else:
            p[0] = ASTNode('forall %s; %s (%s) = %s', [p[2], ASTNode(p[4]), p[6], p[9]])

    def p_nidecl(self, p):
        'nidecl : IDENT AMONG LPAREN termplus RPAREN'
        p[0] = ASTNode('%s among (%s)', [ASTNode(p[1]), p[4]])

    def p_delcaration_type(self, p):
        'declaration : TYPE IDENT options PERIOD'
        if p[3]:
            p[0] = ASTNode('type %s %s.', [ASTNode(p[2]), p[3]])
        else:
            p[0] = ASTNode('type %s.', [ASTNode(p[2])])
    def p_declaration_channel(self, p):
        'declaration : CHANNEL optionplus PERIOD'
        p[0] = ASTNode('channel %s.', [p[2]])
    def p_declaration_free(self, p):
        '''declaration : FREE  optionplus COLON typeid options PERIOD
                       | CONST optionplus COLON typeid options PERIOD'''
        if p[5]:
            p[0] = ASTNode(p[1]+' %s: %s %s.', [p[2], p[4], p[5]])
        else:
            p[0] = ASTNode(p[1]+' %s: %s.', [p[2], p[4]])
    def p_declaration_fun(self, p):
        'declaration : FUN IDENT LPAREN typeids RPAREN COLON typeid options PERIOD'
        if p[8]:
            p[0] = ASTNode('fun %s (%s): %s %s.', [ASTNode(p[2]), p[4], p[7], p[8]])
        else:
            p[0] = ASTNode('fun %s (%s): %s.', [ASTNode(p[2]), p[4], p[7]])
    def p_declaration_letfun(self, p):
        'declaration : LETFUN IDENT args EQUAL pterm PERIOD'
        if p[3]:
            p[0] = ASTNode('letfun %s (%s) = %s.', [ASTNode(p[2]), p[3], p[5]])
        else:
            p[0] = ASTNode('letfun %s () = %s.', [ASTNode(p[2]), p[5]])
    def p_declaration_reduc(self, p):
        'declaration : REDUC reducs options PERIOD'
        if p[3]:
            p[0] = ASTNode('reduc %s %s.', [p[2], p[3]])
        else:
            p[0] = ASTNode('reduc %s.', [p[2]])
    def p_declaration_funreduc(self, p):
        'declaration : FUN IDENT LPAREN typeids RPAREN COLON typeid REDUC reduc2s options PERIOD'
        if p[10]:
            p[0] = ASTNode('fun %s (%s): %s reduc %s %s.', [ASTNode(p[2]), p[4], p[7], p[9], p[10]])
        else:
            p[0] = ASTNode('fun %s (%s): %s reduc %s.', [ASTNode(p[2]), p[4], p[7], p[9]])
    def p_declaration_equation(self, p):
        '''declaration : EQUATION FORALL typedeclplus SEMI term EQUAL term PERIOD
                       | EQUATION term EQUAL term PERIOD'''
        if len(p) == 6:
            p[0] = ASTNode('equation %s = %s.', [p[2], p[4]])
        else:
            p[0] = ASTNode('equation forall %s; %s = %s.', [p[3], p[5], p[7]])
    def p_declaration_pred(self, p):
        '''declaration : PRED IDENT LPAREN typeids RPAREN options PERIOD
                       | PRED IDENT options PERIOD'''
        s = 'pred %s'
        l = [ASTNode(p[2])]
        if len(p) == 8:
            s += ' (%s)'
            l.append(p[4])
        if p[len(p)-2]:
            s += ' %s'
            l.append(p[len(p)-2])
        s += '.'
        p[0] = ASTNode(s, l)
    def p_declaration_table(self, p):
        'declaration : TABLE IDENT LPAREN typeids RPAREN PERIOD'
        p[0] = ASTNode('table %s (%s).', [ASTNode(p[2]), p[4]])
    def p_declaration_let(self, p):
        '''declaration : LET IDENT args LBRAC RECURSIVE INT RBRAC EQUAL process PERIOD
                       | LET IDENT args EQUAL process PERIOD'''
        if len(p) == 7:
            p[0] = ASTProcess(p[2], p[5], p[3])
        else:
            p[0] = ASTProcess(p[2], p[9], p[3], p[6])
    def p_declaration_set(self, p):
        'declaration : SET IDENT EQUAL IDENT PERIOD'
        p[0] = ASTNode('set %s = %s.', [ASTNode(p[2]), p[4]])
    def p_declaration_event(self, p):
        '''declaration : EVENT IDENT LPAREN typeids RPAREN PERIOD
                       | EVENT IDENT PERIOD'''
        if len(p) == 4:
            p[0] = ASTNode('event %s.', [ASTNode(p[2])])
        else:
            p[0] = ASTNode('event %s (%s).', [ASTNode(p[2]), p[4]])
    def p_declaration_query(self, p):
        '''declaration : QUERY typedeclplus SEMI query PERIOD
                       | QUERY query PERIOD'''
        if len(p) == 4:
            p[0] = ASTNode('query %s.', [p[2]])
        else:
            p[0] = ASTNode('query %s; %s.', [p[2], p[4]])
    def p_declaration_noninterf(self, p):
        '''declaration : NONINTERF typedeclplus SEMI nidecls PERIOD
                       | NONINTERF nidecls PERIOD'''
        s = 'noninterf '
        l = []
        if len(p) == 6:
            s += '%s; '
            l.append(p[2])
        if p[len(p)-2]:
            s += '%s'
            l.append(p[len(p)-2])
        s += '.'
        p[0] = ASTNode(s, l)
    def p_declaration_weaksecret(self, p):
        '''declaration : WEAKSECRET IDENT PERIOD
                       | PROBA IDENT PERIOD'''
        p[0] = ASTNode(p[1] + ' %s.', [ASTNode(p[2])])
    def p_declaration_clauses(self, p):
        'declaration : CLAUSES clauses PERIOD'
        p[0] = ASTNode(p[1] + ' %s.', [p[2]])
    def p_declaration_not(self, p):
        '''declaration : NOT typedeclplus SEMI gterm PERIOD
                       | NOT gterm PERIOD
                       | NOUNIF typedeclplus SEMI nounifdecl PERIOD
                       | NOUNIF nounifdecl PERIOD
                       | ELIMTRUE failtypedeclplus SEMI term PERIOD
                       | ELIMTRUE term PERIOD'''
        if len(p) == 4:
            p[0] = ASTNode(p[1] + ' %s.', [p[2]])
        else:
            p[0] = ASTNode(p[1] + ' %s; %s.', [p[2], p[4]])
    def p_declaration_param(self, p):
        'declaration : PARAM optionplus options PERIOD'
        if p[3]:
            p[0] = ASTNode('param %s %s.', [p[2], p[3]])
        else:
            p[0] = ASTNode('param %s.', [p[2]])
    # FIXME: ignore this
    def p_declaration_proof(self, p):
        'declaration : PROOF LCURL RCURL'
        pass
    def p_declaration_def(self, p):
        'declaration : DEF IDENT LPAREN typeids RPAREN LCURL declarations RCURL'
        s = 'def %s ('
        l = [ASTNode(p[2])]
        if p[4]:
            s += '%s'
            l.append(p[4])
        s += ') { '
        if p[7]:
            s += '%s'
            l.append(p[7])
        s += ' }'
        p[0] = ASTNode(s, l)
    def p_declaration_expand(self, p):
        'declaration : EXPAND IDENT LPAREN typeids RPAREN PERIOD'
        if p[4]:
            p[0] = ASTNode('expand %s (%s).', [ASTNode(p[2]), p[4]])
        else:
            p[0] = ASTNode('expand %s ().', [ASTNode(p[2])])

    def p_query_gterm(self, p):
        'query : gterm SEMI query'
        p[0] = ASTNode('%s;\n%s', [p[1], p[3]])
    def p_query_event(self, p):
        '''query : PUTBEGIN EVENT COLON optionplus SEMI query
                 | PUTBEGIN EVENT COLON optionplus
                 | PUTBEGIN INJEV COLON optionplus SEMI query
                 | PUTBEGIN INJEV COLON optionplus'''
        if len(p) == 5:
            p[0] = ASTNode('putbegin ' + p[2] + ': %s', [p[4]])
        else:
            p[0] = ASTNode('putbegin ' + p[2] + ': %s;\n%s', [p[4], p[6]])
    def p_gterm_new(self, p):
        '''gterm   : NEW IDENT LBRAC gbinding RBRAC
                   | NEW IDENT LBRAC RBRAC
                   | NEW IDENT
           gformat : NEW IDENT LBRAC fbinding RBRAC
                   | NEW IDENT LBRAC RBRAC
                   | NEW IDENT'''
        s = 'new %s'
        l = [ASTNode(p[2])]
        if len(p) > 3:
            s += '['
            if len(p) == 6:
                s += '%s'
                l.append(p[4])
            s += ']'
        p[0] = ASTNode(s, l)
    def p_gterm_let(self, p):
        '''gterm   : LET IDENT EQUAL gterm   IN gterm
           gformat : LET IDENT EQUAL gformat IN gformat'''
        p[0] = ASTNode('let %s = %s in %s', [ASTNode(p[2]), p[4], p[6]])
    def p_gbinding_excl(self, p):
        '''gbinding : EXCL INT EQUAL gterm   SEMI gbinding
                    | EXCL INT EQUAL gterm
           fbinding : EXCL INT EQUAL gformat SEMI fbinding
                    | EXCL INT EQUAL gformat'''
        if len(p) == 5:
            p[0] = ASTNode('! %s = %s', [ASTNode(str(p[2])), p[4]])
        else:
            p[0] = ASTNode('! %s = %s; %s', [ASTNode(str(p[2])), p[4], p[6]])
    def p_gbinding_ident(self, p):
        '''gbinding : IDENT EQUAL gterm   SEMI gbinding
                    | IDENT EQUAL gterm
           fbinding : IDENT EQUAL gformat SEMI fbinding
                    | IDENT EQUAL gformat'''
        if len(p) == 5:
            p[0] = ASTNode('%s = %s', [ASTNode(p[2]), p[4]])
        else:
            p[0] = ASTNode('%s = %s; %s', [ASTNode(p[2]), p[4], p[6]])

    def p_nounifdecl_let(self, p):
        'nounifdecl : LET IDENT EQUAL gformat IN nounifdecl'
        p[0] = ASTNode('let %s = %s in %s', [ASTNode(p[2]), p[4], p[6]])
    def p_nounifdecltmp_phase(self, p):
        '''nounifdecltmp : IDENT LPAREN gformats RPAREN PHASE INT
                         | IDENT LPAREN gformats RPAREN'''
        if len(p) == 5:
            p[0] = ASTNode('%s (%s)', [ASTNode(p[1]), p[3]])
        else:
            p[0] = ASTNode('%s (%s) phase %s', [ASTNode(p[1]), p[3], ASTNode(str(p[6]))])
    def p_nounifdecl_phase(self, p):
        '''nounifdecl : nounifdecltmp SLASH INT
                      | nounifdecltmp'''
        p[0] = p[1]
        if len(p) == 4:
            p[0].format += ' /%s'
            p[0].args.append(ASTNode(str(p[3])))
    def p_gformat_star(self, p):
        'gformat : STAR IDENT'
        p[0] = ASTNode('* %s', [ASTNode(p[2])])

    def p_clausestmp(self, p):
        'clausestmp : FORALL failtypedeclplus SEMI clause'
        p[0] = ASTNode('forall %s; %s', [p[2], p[4]])
    def p_clauses(self, p):
        'clauses : clausestmp SEMI clauses'
        p[0] = ASTNode('%s; %s', [p[1], p[3]])

    # FIXME: only accept 0
    def p_process_0(self, p):
        'process : INT'
        p[0] = ASTNode('0')
    def p_process_yield(self, p):
        'process : YIELD'
        p[0] = ASTNode('yield')
    def p_process_call(self, p):
        '''process : IDENT LPAREN pterms RPAREN
                   | IDENT'''
        s = '%s ('
        l = [ASTNode(p[1])]
        if len(p) > 2 and p[3]:
            s += '%s'
            l.append(p[3])
        s += ')'
        p[0] = ASTNode(s, l)
    def p_process_paren(self, p):
        'process : LPAREN process RPAREN'
        p[0] = ASTNode('(\n%s\n%s', [p[2], ASTNode(')')], {0: 0, 1: 0})
    def p_process_paral(self, p):
        'process : process PIPE process %prec PROCESS_PIPE'
        p[0] = ASTNode('%s | %s', [p[1], p[3]])
    def p_process_rep(self, p):
        'process : EXCL process %prec PROCESS_EXCL'
        p[0] = ASTNode('! %s', [p[2]])
    def p_process_repident(self, p):
        'process : EXCL IDENT LEQ IDENT'
        p[0] = ASTNode('! %s <= %s', [ASTNode(p[2]), ASTNode(p[4])])
    def p_process_new(self, p):
        'process : NEW IDENT COLON typeid SEMI process'
        p[0] = ASTNode('new %s: %s;\n%s', [ASTNode(p[2]), p[4], p[6]], {2: 0})
    def p_process_if(self, p):
        '''process : IF pterm THEN process ELSE process %prec PROCESS_ITE
                   | IF pterm THEN process %prec PROCESS_IT'''
        if len(p) == 5:
            p[0] = ASTNode('if %s then\n%s', [p[2], p[4]], {1: 1})
        else:
            p[0] = ASTNode('if %s then\n%s\n%s\n%s', [p[2], p[4], ASTNode('else'), p[6]], {1: 1, 2: 0, 3: 1})
    def p_process_inout(self, p):
        '''process : IN  LPAREN pterm COMMA pattern RPAREN SEMI process
                   | IN  LPAREN pterm COMMA pattern RPAREN
                   | OUT LPAREN pterm COMMA pterm   RPAREN SEMI process
                   | OUT LPAREN pterm COMMA pterm   RPAREN'''
        if len(p) == 7:
            p[0] = ASTNode(p[1]+' (%s, %s)', [p[3], p[5]])
        else:
            p[0] = ASTNode(p[1]+' (%s, %s);\n%s', [p[3], p[5], p[8]], {2: 0})
    def p_process_let(self, p):
        '''process : LET pattern      EQUAL    pterm IN process ELSE process %prec PROCESS_LET_ELSE
                   | LET pattern      EQUAL    pterm IN process %prec PROCESS_LET_IN
                   | LET pattern      EQUAL    pterm %prec PROCESS_LET
                   | LET typedeclplus SUCHTHAT pterm IN process ELSE process %prec PROCESS_LET_ELSE
                   | LET typedeclplus SUCHTHAT pterm IN process %prec PROCESS_LET_IN
                   | LET typedeclplus SUCHTHAT pterm %prec PROCESS_LET'''
        s = 'let %s '+p[3]+' %s'
        l = [p[2], p[4]]
        a = {}
        if len(p) > 5:
            s += ' in\n%s'
            l.append(p[6])
            a[2] = 0
            if len(p) > 7:
                s += '\n%s\n%s'
                l.append(ASTNode('else'))
                l.append(p[8])
                a[2] = 1
                a[3] = 0
                a[4] = 1
        p[0] = ASTNode(s, l, a)
    def p_process_insert(self, p):
        '''process : INSERT IDENT LPAREN pterms RPAREN SEMI process
                   | INSERT IDENT LPAREN pterms RPAREN'''
        s = 'insert %s ('
        l = [ASTNode(p[2])]
        a = {}
        if p[4]:
            s += '%s'
            l.append(p[4])
        s += ')'
        if len(p) > 6:
            s += ';\n%s'
            l.append(p[7])
            a[len(l)-1] = 0
        p[0] = ASTNode(s, l, a)
    def p_process_getsuchthat(self, p):
        '''process : GET IDENT LPAREN patterns RPAREN SUCHTHAT pterm IN process ELSE process
                   | GET IDENT LPAREN patterns RPAREN SUCHTHAT pterm IN process
                   | GET IDENT LPAREN patterns RPAREN SUCHTHAT pterm'''
        s = 'get %s ('
        l = [ASTNode(p[2])]
        a = {}
        if p[4]:
            s += '%s'
            l.append(p[4])
        s += ') suchthat %s'
        l.append(p[7])
        if len(p) > 8:
            s += ' in\n%s'
            l.append(p[9])
            a[len(l)-1] = 0
            if len(p) > 10:
                s += '\n%s\n%s'
                l.append(ASTNode('else'))
                l.append(p[11])
                a[len(l)-1] = 1
                a[len(l)-2] = 0
                a[len(l)-3] = 1
        p[0] = ASTNode(s, l, a)
    def p_process_get(self, p):
        '''process : GET IDENT LPAREN patterns RPAREN IN process ELSE process
                   | GET IDENT LPAREN patterns RPAREN IN process
                   | GET IDENT LPAREN patterns RPAREN'''
        s = 'get %s ('
        l = [ASTNode(p[2])]
        a = {}
        if p[4]:
            s += '%s'
            l.append(p[4])
        if len(p) > 6:
            s += ' in\n%s'
            l.append(p[7])
            a[len(l)-1] = 0
            if len(p) > 8:
                s += '\n%s\n%s'
                l.append(ASTNode('else'))
                l.append(p[9])
                a[len(l)-1] = 1
                a[len(l)-2] = 0
                a[len(l)-3] = 1
        p[0] = ASTNode(s, l, a)
    def p_process_eventargs(self, p):
        '''process : EVENT IDENT LPAREN pterms RPAREN SEMI process
                   | EVENT IDENT LPAREN pterms RPAREN'''
        s = 'event %s ('
        l = [ASTNode(p[2])]
        a = {}
        if p[4]:
            s += '%s'
            l.append(p[4])
        s += ')'
        if len(p) > 6:
            s += ';\n%s'
            l.append(p[7])
            a[len(l)-1] = 0
        p[0] = ASTNode(s, l, a)
    def p_process_event(self, p):
        '''process : EVENT IDENT SEMI process
                   | EVENT IDENT'''
        if len(p) == 3:
            p[0] = ASTNode('event %s', [ASTNode(p[2])])
        else:
            p[0] = ASTNode('event %s;\n%s', [ASTNode(p[2]), p[4]], {1: 0})
    def p_process_phase(self, p):
        '''process : PHASE INT SEMI process
                   | PHASE INT'''
        if len(p) == 3:
            p[0] = ASTNode('phase %s', [ASTNode(str(p[2]))])
        else:
            p[0] = ASTNode('phase %s;\n%s', [ASTNode(str(p[2])), p[4]], {1: 0})

    def p_error(self, p):
        if p is None:
            s = 'Unexpected end of file'
        else:
            s = 'Syntax error on line %d:\n' % p.lineno
            i = p.lineno
            pos = 0
            for l in open(self.filename):
                i -= 1
                if i == 0:
                    s += l
                    s += ' '*(p.lexpos-pos)
                    s += '^\n'
                    break
                pos += len(l)
        print s
        #raise SyntaxError(s)

    def buildParser(self, **kwargs):
        self.yacc = yacc.yacc(module=self, **kwargs)

    def parse_file (self, filename):
        if self.lexer is None or self.yacc is None:
            sys.exit('Please build lexer and parser')
        self.filename = filename
        content_file = open(filename, 'r')
        content = content_file.read()
        root = self.yacc.parse(content)
        content_file.close()
        if root.__class__.__name__ == 'ASTMain' or root.__class__.__name__ == 'ASTMainEquiv':
            return root
        return None

bitrepr = ['O', 'I']
def bitlist (n, w):
    return [bitrepr[int(c)] for c in ("{0:0"+str(w)+"b}").format(n)]

def parse_file (filename):
    m = PiTypeLexer()
    m.buildLexer()
    tmpdir = os.path.dirname(os.path.abspath(__file__))+'/tmp'
    m.buildParser(outputdir=tmpdir, debug=0)
    root = m.parse_file(filename)
    return root
