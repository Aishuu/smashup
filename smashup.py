#! /usr/bin/env python2

#  This software is a python script that enables to translate assembly
#  instructions into a ProVerif specification that ProVerif is able to
#  prove.

#  This software is governed by the CeCILL  license under French law and
#  abiding by the rules of distribution of free software.  You can  use,
#  modify and/ or redistribute the software under the terms of the CeCILL
#  license as circulated by CEA, CNRS and INRIA at the following URL
#  "http://www.cecill.info".

#  As a counterpart to the access to the source code and  rights to copy,
#  modify and redistribute granted by the license, users are provided only
#  with a limited warranty  and the software's author,  the holder of the
#  economic rights,  and the successive licensors  have only  limited
#  liability.

#  In this respect, the user's attention is drawn to the risks associated
#  with loading,  using,  modifying and/or developing or reproducing the
#  software by the user in light of its specific status of free software,
#  that may mean  that it is complicated to manipulate,  and  that  also
#  therefore means  that it is reserved for developers  and  experienced
#  professionals having in-depth computer knowledge. Users are therefore
#  encouraged to load and test the software's suitability as regards their
#  requirements in conditions enabling the security of their systems and/or
#  data to be ensured and,  more generally, to use and operate it in the
#  same conditions as regards security.

#  The fact that you are presently reading this means that you have had
#  knowledge of the CeCILL license and that you accept its terms.

import sys
import argparse
import os

argparser = argparse.ArgumentParser(description='Generate ProVerif specification from assembly code')
argparser.add_argument("infile", metavar='infile', 
                    help='SW file to process')
argparser.add_argument("--hdl", dest='hdlfile', metavar='file',
                    help='HW description file')
argparser.add_argument("-o", dest='outfile', metavar='file',
                    help='output file')
argparser.add_argument("-out", dest='format', choices=['horn', 'horntype', 'pi', 'pitype'],
                    metavar='format',
                    help='output format (horn [default], horntype, pi or pitype)')
argparser.add_argument("-s", dest='section', metavar='name',
                    help='Section to analyze')
argparser.add_argument("--clauses", dest='clauses', action='store_const',
                    const=True, default=False,
                    help='print horn clauses')
argparser.add_argument("--rules", dest='rules', action='store_const',
                    const=True, default=False,
                    help='print rules during unification')

args = argparser.parse_args()

exts = {'horn': 'horn', 'horntype': 'horntype', 'pi': 'pi', 'pitype': 'pv'}

if not os.path.exists(args.infile):
    sys.exit("File '%s' doesn't exist." % args.infile)

hdlfile = args.hdlfile
if hdlfile is None:
    hdlfile = '%s.hdl' % args.infile.rsplit('.', 1)[0]
if not os.path.exists(hdlfile):
    sys.exit("File '%s' doesn't exist." % hdlfile)

form = args.format
if form is None:
    if args.outfile is not None and '.' in args.outfile:
        e = args.outfile.rsplit(".", 1)[1]
        for t, ext in exts.iteritems():
            if ext == e:
                form = t
                break
if form is None:
    form = 'horn'
    
# TODO: implement
if form == 'horn' or form == 'horntype' or form == 'pi' :
    sys.exit("Format '%s' not implemented." % form)

if form == 'horn':
    import parser.horn as parser
elif form == 'horntype':
    import parser.horntype as parser
elif form == 'pi':
    import parser.pi as parser
elif form == 'pitype':
    import parser.pitype as parser
else:
    sys.exit('Internal Error...')
import parser.s43 as asparser
import parser.hdl as hdl

outfile = args.outfile
if outfile is None:
    outfile = 'a.out'
    if os.path.exists(outfile):
        sys.exit("Default output file '%s' already exists." % outfile)

pgm = asparser.parse_file (args.infile)
if pgm is None:
    sys.exit('Error when parsing input file')
decls = pgm.getSection(args.section)
labels = []
instr = []
index = 0
for d in decls:
    if d.isLabel():
        labels.append(d)
        d.addr = index
    elif d.isInstruction():
        d.index = index
        instr.append(d)
        index += 1
instr.append(asparser.InstrDecl('END', []))
for d in instr:
    nops = []
    for o in d.ops:
        if o.isLabel():
            n = None
            for l in labels:
                if l.name == o.name:
                    # FIXME
                    n = asparser.ImmOp(l.addr)
                    break
            if n is None:
                sys.exit('Undeclared label...')
            nops.append(n)
        else:
            nops.append(o)
    d.ops = nops

rcount, bitwidth, memrange, mods = hdl.readHDLFile (hdlfile)
for m in mods:
    m.instantiate()

declarations = []
declarations.append(parser.ASTComment    (['(* Types *)']))
for m in mods:
    declarations += m.typeDecl(parser)

for m in mods:
    declarations += m.funcDecl(parser, rcount, bitwidth, memrange)

declarations.append(parser.ASTComment(['', '(* State Manipulation *)']))
stateAll = [m.state(parser) for m in mods]
stateAll = [(m.modName, stateAll[i][0], stateAll[i][1]) for i, m in enumerate(mods)]
stateIn  = [s[1] for s in stateAll]
stateOutPlusOne = [(s[0], s[2]) for s in stateAll]
stateOut = ['r0, ' + ', '.join(s[1].split(', ')[1:]) if s[0] == 'CPU' else s[1] for s in stateOutPlusOne]
typeIn = ', '.join([s.split(': ')[1] for s in ', '.join(stateIn).split(', ')])
declarations.append(parser.ASTNode        ('fun dummyenc (' + typeIn + '): bitstring [private].'))
declarations.append(parser.ASTNode        ('reduc forall ' + ', '.join(stateIn) + '; dummydec(dummyenc(' + ', '.join(stateOut) + ')) = (' + ', '.join(stateOut) + ') [private].'))

declarations.append(parser.ASTComment(['', '(* Variables *)']))
declarations.append(parser.ASTNode        ('free secret: int [private].'))
declarations.append(parser.ASTNode        ('free ch: channel.'))

declarations.append(parser.ASTComment(['', '(* Queries *)']))
declarations.append(parser.ASTNode        ('query attacker(secret).'))

if args.clauses or args.rules:
    declarations.append(parser.ASTComment(['', '(* Configuration *)']))
if args.clauses:
    declarations.append(parser.ASTComment(['set verboseClauses = short.']))
if args.rules:
    declarations.append(parser.ASTComment(['set verboseRules = true.']))

declarations.append(parser.ASTComment(['', '(* Processes *)']))

module_process = []
for id, m in enumerate(mods):
    p, d = m.processDecl(parser, rcount, bitwidth, memrange, stateAll, id)
    declarations += d
    module_process += p

instr_coll = {}
for pc, i in enumerate(instr):
    name = None
    for id, m in enumerate(mods):
        name, node = m.createInstr(parser, i, rcount, bitwidth, memrange, list(stateOutPlusOne), id)
        if name is not None:
            break
    if name is None:
        raise Exception ('Illegal instruction: %s' % i.instr)
    if name in instr_coll:
        instr_coll[name][1].append(pc)
    else:
        instr_coll[name] = [node, [pc]]
for k in instr_coll:
    declarations.append(parser.ASTNode('let '+k+' =\n    in (ch, enc_state: bitstring);\n    let ('+', '.join(stateIn) + ') = dummydec(enc_state) in\n\n    if ' + ' ||\n       '.join(['r0 = O'+str(i)+'()' for i in instr_coll[k][1]]) + ' then\n\n    %s.\n', [instr_coll[k][0]], {0: 0}))

stateInitIn, stateInitOut = zip(*[list(m.stateInit(parser)) for m in mods])
mainproc = parser.ASTNode(' | '.join(['(!'+k+')' for k in instr_coll.keys()+module_process]) + ' |\n\n    (in (ch, (' + ', '.join(stateInitIn) + '));\n    out (ch, dummyenc(' + ', '.join(stateInitOut) + ')))' )
    
root = parser.ASTMain(parser.ASTSequence(declarations, '', '', '\n'), mainproc)
root.duplicate()
with open(outfile, 'w') as outfd:
    outfd.write(root.describe(0))
    outfd.close()
